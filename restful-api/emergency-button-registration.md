# Emergency Button Registration

## Endpoint

< to be defined >

## Purpose

Handle registration request from Emergency application. For Emergency Button device, this registration will be done by system administrator using portal application.

## Caller

- Emergency Button device
- Emergency Button on smartphone application

## Request Payload

```
{
    "type": "emergency-button-registration",
    "content":{
        "fullname": <string>,
        "phone_1":<string>,
        "phone_2":<string>,
        "ktp_number":<string>
        "mr_number":<string||null>
        "static_address":<string||null>,
        "device_type":<enum>
    }
  
}
```

## Sample Data
![image request](../../out-plantuml/Documents/restful-api/emergency-button-registration/emergency-button-registration-request.png)

## Response Payload

- on success

```
{
    "type":"ack",
    "content":
    {
        "success":true,
        "caller_verification_code":<string>
    }
    
}
```



- on failure

```
{
    "type":"nack",
    "content":
    {
        "success":false,
        "error_message":<string>
        "http_status": <int||null>
    }
    
}
```
* Possible Failure
  * http_status: 400 Bad Request
  * http_status: 409 Conflict (phone number already registered)
  * http_status: 412 Precondition failed. 1 or more mandatory field has null value.
  * etc.

## Sample Data
- on success
![image response-ok](../../out-plantuml/Documents/restful-api/emergency-button-registration/emergency-button-registration-response-success.png)


- on failure
![image response-faile](../../out-plantuml/Documents/restful-api/emergency-button-registration/emergency-button-registration-response-fail.png)