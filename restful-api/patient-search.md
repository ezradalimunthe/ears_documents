# Patient Seach

## Endpoint

SIM RS

## Purpose

Search patient based on parameters

## Caller

Dispatch Management Application

## Request Payload

```
{
    "mr_number": <string|null>
    "patient_name":<string|null>
    "address":<string|nulll>
}
```

## Response Payload

```
[
    {
        "mr_number": <string|null>
        "patient_name":<string|null>
        "address":<string|nulll>
    },
    {
        "mr_number": <string|null>
        "patient_name":<string|null>
        "address":<string|nulll>
    }
]
```
