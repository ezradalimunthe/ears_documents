# Dynamic Form Structure

## Base Structure

```json
{
  "version": "<string>",
  "formName": "<string>",
  "children": "array of component"
}
```

version: Dynamic Form Version
formName: Name of the Form
children: Array of components or Null value

## Components

Possible components:

### Section

Group of elements

```json
{
  "type": "Section",
  "text": "<string>",
  "children": "array of component"
}
```

### SubSection

Group of elements

```json
{
  "type": "SubSection",
  "text": "<string>",
  "children": "array of component"
}
```

### ReadOnly

```json
{
  "type": "ReadOnly",
  "name": "<string>",
  "text": "<string>",
  "value": "<string>"
}
```

### InputText

Rendered as input textbox

```json
{
  "type": "InputText",
  "name": "<string>",
  "text": "<string>",
  "value": "<string>",
  "defaultValue": "<string>",
  "prepend": "<string>",
  "append": "<string>"
}
```

### InputHidden

```json
{
  "type": "InputHidden",
  "name": "<string>",
  "value": "<string>",
  "defaultValue": "<string>"
}
```

### InputNumeric

Render as input for numeric value.

```json
{
  "type": "InputNumeric",
  "name": "<string>",
  "text": "<string>",
  "value": "<string>",
  "defaultValue": "<string>",
  "prepend": "<string>",
  "append": "<string>"
}
```

### InputDate

Render as input for datetime;
inputFormat

```json
{
  "type": "InputDate",
  "text": "<string>",
  "name": "<string>",
  "value": "<string format yyyy-MM-dd hh:mm:ss>",
  "defaultValue": "<string format yyyy-MM-dd hh:mm:ss>"
}
```

### InputCheckbox

Render as checkbox ; Parent can be any components that has property children.

```json
{
  "type": "InputCheckbox",
  "text": "<string>",
  "value": "<string>"
}
```

### CheckboxGroup

Render as section of checkbox;children must be array of object with attribute text and value
Attribute **value** must be boolean represented by 0 or 1.

```json
{
  "type": "CheckboxGroup",
  "text": "<string>",
  "children": [
    {text:"", value:0}
  ]
}
```

### InputRadio

Rendered as radio input. Must be rendered on RadioGroup

```json
{
  "type": "InputRadio",
  "value": "<string>",
  "text": "<string>"
}
```

### RadioGroup

```json
{
  "type": "RadioGroup",
  "name": "<string>",
  "text": "<string>",
  "value": "<string, child selected value>",
  "children": "array of InputRadio"
}
```

### inputSelect

Rendered as dropdownlist

```json
{
  "type": "inputSelect",
  "text": "CPR in progress on arrival by",
  "value": "",
  "options": [
      "",
      "Citizen",
      "Firefighter",
      "Officer, First Responder"
  ]
}
```


