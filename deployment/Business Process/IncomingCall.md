# Incoming Call

Actor: 
1. Bystander, the caller
2. Call center operator

Bystander make a call to call center and Call center answer the call and determine the call subject

There are 7 possible call subject
1. Emergency Call
2. Ambulance Booking
3. ETC Telephone Consultation
4. Not Relevant Call Transfer
5. Not Relevant Call Ended
6. Ambulance Booking Cancelation
7. Asking for Emergency call status

When answering the call, Call Center operator make a call record and save it to **case_masters** table

Data inserted to case_masters
1. case_prefix, 
2. case_type, 
3. time_phone
4. caller_name
5. source_call
6. status_id
7. username


Required when subject is _ETC Telephone Consultation_, _Not Relevant Call Transfered_, and _Not Relevant Call Ended_
1. Notes
 
Required when subject is _ETC Telephone Consultation_, and _Not Relevant Call Transfered_ 
1. Forward To

