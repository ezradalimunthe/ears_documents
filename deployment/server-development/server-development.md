# Development server

Untuk development, server yang dipakai adalah server Halotec (office.halotec-indonsia.com).

# URI

## Backend server

1. SSO
   1. Url: http://office.halotec-indonesia.com:8102
   2. webroot : //var/www/projects/ears/ears_sso
   3. git : https://ezradalimunthe@bitbucket.org/ezradalimunthe/ears_sso.git
2. EARS Api
   1. Url : http://office.halotec-indonesia.com:8103
   2. webroot: //var/www/projects/ears/ears_api
   3. git: https://ezradalimunthe@bitbucket.org/ezradalimunthe/ears_api.git
3. EARS Notif
   1. Url : http://office.halotec-indonesia.com:8104
   2. webroot: //var/www/projects/ears/ears_notif_server
   3. git: https://ezradalimunthe@bitbucket.org/ezradalimunthe/ears_notif_server.git

4. crontab
```
* * * * * cd /var/www/projects/ears/ears_api && php artisan schedule:run >> /dev/null 2>&1
```

## Front End server

1. EARS Portal
   1. Url : http://office.halotec-indonesia.com:8105
   2. webroot: //var/www/projects/ears/ears_portal
   3. git: https://ezradalimunthe@bitbucket.org/ezradalimunthe/ears_portal.git
2. EARS Ambulance Monitor:
   1. Url: http://office.halotec-indonesia.com:8106
   2. webroot: //var/www/projects/ears/ears_er_room
   3. git: https://ezradalimunthe@bitbucket.org/ezradalimunthe/ears_er_room.git
3. EARS DISPATCH MGT:
   1. Url: http://office.halotec-indonesia.com:8107
   2. webroot: //var/www/projects/ears/ears_dispatchmgt
   3. git : https://ezradalimunthe@bitbucket.org/ezradalimunthe/dispatchmanagement.git

# Update Application on Server Development

1. Make sure master repository of application to be updated is lastest version
2. Open Jenkins http://office.halotec-indonesia.com:9090 and login
3. Jenkin account
   1. Username: halotec
   2. password: halotec123
4. On dashboard there is list of application available to update
   1. Ears-SSO-Development for SSO app
   2. Ears-API-Development for Ears Api app.
   3. Ears-DispatchManagement-Development for Dispatch Management app.
   4. Ears-WebPortal-Development for Webportal app
   5. EARS Notif is not availabel to update from jenkins at this moment.
6. Click most right icon on list after application name to update the application
7. During updating the app, on bottom left side menu, indicator of update progress is shown until update done. 