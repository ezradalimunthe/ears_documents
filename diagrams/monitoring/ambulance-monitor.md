# Ambulance Monitor

## Brief Description

Ambulance monitor adalah aplikasi yang menampilkan posisi rumahsakit dan ambulance yang dimiliki oleh rumah sakit tersebut pada peta. Aplikasi ini merupakan bagian dari sistem
Emergency Ambulance Response System dan tergantung pada subsistem yang ada pada sistem tersebut.
Untuk memakai aplikasi, user harus melakukan login agar aplikasi dapat mengidentifikasi rumah sakit tempat user berada. Bila user tidak memiliki rumah sakit (seperti dispatcher dan super executive), maka aplikasi akan menampilkan seluruh data ambulance dengan fokus pada rumah sakit pertama pada daftar rumah sakit yang diberikan oleh sistem.

## Spesification

Development platform: Vue.js
Type: Web application
Map platform: Google Map

## Application Layout

Layout tampilan terdiri dari 2 section, yaitu section peta dan section navigasi.

1. Section peta, memakai lebih dari 70% tampilan berada pada sisi kiri layout.

   Pada saat pertama aplikasi berjalan, peta menampilkan icon representasi rumah sakit di tengah-tengah peta dan icon representasi posisi ambulance sesuai posisi ambulance terakhir di database.

   Rumah sakit yang ditampilkan adalah rumah sakit yang dimiliki oleh login user. Bila user tidak memiliki rumah sakit, maka rumah sakit yang terpilih adalah rumah sakit yang pertama ditampilkan pada dropdown di section navigasi.

   Level zoom pada map menampilkan radius antara 8km-10km dari rumah sakit.

   Klik pada icon ambulance akan membuat ambulance berada di tengah peta dan zoom level akan berubah dan menampilkan radius antara 700-1000 m sehingga user dapat melihat posisi ambulance lebih akurat (dapat melihat nama jalan dsb.)

   Klik pada icon ambulance juga akan menampilkan keterangan mengenai ambulance pada tampilan navigasi.

2. Navigasi, berada di sisi kanan layout sejajar dengan peta.

   Navigasi menampilkan

   1. Dropdown yang berisi nama-nama rumah sakit. Dropdown ini hanya tampil bila user yang login adalah user yang tidak bekerja di rumah sakit (seperti dispatcher dan Super Executive)
      Bila dropdown dipilih, maka peta akan menampilkan (zoom) rumah sakit yang dipilih.

   2. Daftar Ambulance yang ada di rumah sakit tersebut. Data yang ditampilkan adalah Kode ambulance dan status (Available, Leaving for scene, dsb)

   Klik pada ambulance pada list akan mengubah fokus peta ke ambulance yang dipilih dengan zoom level yang menampilkan radius antara 700m-1000m dari ambulance.

   3. Legenda warna status ambulance

## Interaksi

### Server

1. SSO server
   SSO server digunakan untuk melakukan login ke dalam sistem.
2. Notification Server
   Digunakan untuk mendapatkan posisi ambulance untuk ditampilkan di peta. Protoco

### Payload

1.  Login to SSO

    **End point** : <SSOServer>/api/v1/auth/authenticate

    **Protocal**: Http POST

    **Request Payload**: Json

        ```json
        {
            "username": "<username>",
            "password": "<password>"
        }
        ```

    **Response Payload**: JSON on success Http 200

    ```json
    {
      "token": "<JWT TOKEN>"
    }
    ```

    **Response Payload**: JSON on fail Http 400

    ```json
    {
      "errorMessages": ["Login credentials are invalid."],
      "exception": "LoginValidationException",
      "source": null
    }
    ```

2.  User Information

    **End point** : \<SSOServer\>/api/v1/auth/me

    **Protocal**: Http GET

    **Http Header**: Authorization: Bearer <JWT token>

    **Request Payload**: No payload
    **Response Payload**: JSON on success Http 200

    ```json
    {
      "username": "<username>",
      "name": "<name>",
      "email": "<email>",
      "role_id": 15,
      "hospital_id": 0,
      "hospital_code": "<hospital_code>",
      "hospital_name": "<hospital_name>",
      "hospital_latitude": 0,
      "hospital_longitude": 0
    }
    ```

    **Response Payload**: JSON on fail Http 401

    ```json
    {
      "errorMessages": ["The token could not be parsed from the request."],
      "exception": "Tymon\\JWTAuth\\Exceptions\\JWTException",
      "source": "<sso_servername>"
    }
    ```

3.  Login to Notification Server

    **SENDER** : Application

    **Protocol** : websocket

    **Sender Payload**: Json

    ```json
    {
      "type": "intro",
      "content": {
        "label": "hospital_name",
        "app_code": "web_monitor",
        "hospital_code": "hospital_code"
      }
    }
    ```

    **Notes**

    - type value must be "intro"
    - content value must be object
    - content.label value is hospital_name (gathered from user information, see point 2 above ) or null.
    - content.app_code must be "web_monitor"
    - content.hospital_code is hospital_code (gathered from user information, see point 2 above) or null

4.  Received Ambulance Position

    **Sender** : Notification Server

    **Protocol** : websocket

    **Payload**: Json

    ```json
    {
      "type": "list-ambulance-position",
      "content": [
        {
          "hospital_code": "hospital_code",
          "ambulances": [
            {
              "code": "ambulance_code",
              "latitude": 0,
              "longitude": 0,
              "position_status": {
                "label": "Available",
                "color": "#ff00ff"
              },
              "lastUpdate": "<string date utc>"
            }
          ]
        }
      ]
    }
    ```

## Architecture

Sequence on update Ambulance Position 

![send-position](../../out-plantuml/diagrams/monitoring/intro/Send%20position.png)


Sequences

1. Driver app send position to Api Server
   
   **End Point**: /api/v1/ambulance-position/stream
   
   **Protocol** : Http Post


   **Request payload** : JSON

   ````json
   {
    "ambulance_code": "SMME-001",
    "hospital_code":"SMME",
    "latitude": 1.54686496466,
    "longitude": 98.25645444,
    "status_id": 2,
    "dispatch_order_id":1
    }   

   ````

  

  2. Api server update ambulance position on database
  3. Api server return response 

 **Response payload**: HTTP 200

  ````json
    {
      "message": "data updated"
      }   

  ````

  **Response Payload**: JSON on fail Http 500

  ```json
    {
      "errorMessages": ["MESSAGE"],
      "exception": "exception",
      "source": null
    }
  ```

  4. Send ambulance position to notification server

  ````json
    {
      "type":"update-ambulance-position",  
      "content": [
        {
            "hospital_code": "hospital_code",
            "ambulances": [
            {
                "code": "ambulance_code",
                "latitude": 0,
                "longitude": 0,
                "from": "Rumah Sakit",
                "to":"Jalan Kasuari No 77 Medan",
                "position_status": {
                "label": "Available",
                "color": "#ff00ff"
                },
                "lastUpdate": "<string date utc>"
            }
            ]
        }
     ]
    }   

  ````

  5.  Send ambulance position to monitor
   
      Message dikirimkan dari notification server ke app monitor. 

      Notif server melakukan filtering terlebih dahulu atas koneksi websocket, sehingga penerima message hanyalah koneksi yang memiliki attribute "app_code" bernilai "app_monitor".
      Untuk koneksi yang attribute "hospital_code" tidak sama dengan null, maka hanya payload yang hospital_code yang sama yang diteruskan ke app monitor.

      Payload yang dikirim dari notif server ke monitor

     
  ```json
  {
    "type": "list-ambulance-position",
    "content": [
      {
        "hospital_code": "hospital_code",
        "ambulances": [
          {
            "code": "ambulance_code",
            "latitude": 0,
            "longitude": 0,
            "from": "Rumah Sakit",
            "to":"Jalan Kasuari No 77 Medan",
            "position_status": {
              "label": "Available",
              "color": "#ff00ff"
            },
            "lastUpdate": "<string date utc>"
          }
        ]
      }
    ]
  }
  ```

| Title    | ambulance monitor doc      |
| -------- | -------------------------- |
| Version  | 1.0                        |
| Date     | 2022-06-13 10:00           |
| Revision | 2                          |
| Author   | ezra@halotec-indonesia.com |

Note:
1. Change payload Driver app send position to Api Server
2. Change payload Send ambulance position to monitorSend ambulance position to monitor