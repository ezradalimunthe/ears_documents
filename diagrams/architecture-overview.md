# SIRADAR

## About

SIRADAR is ..

## Overview

<img src = "../../out-plantuml/Documents/diagrams/architecture-1/architecture-1.png" height="700px">

### Users

1. **_By Stander_**

   Users that makes emergency call to call center.

   Requirements:

   1. Makes an emergency calls using Emergency Button on smartphone application or an Emergency Button device.
   2. Books / reserves an ambulance
   3. Calls Ambulance Call Center.

2. **_Call center Operator_**

   Organization user works as call center operator.

   Requirements:

   1. Fill daily checklist at beginning of work shift.
   2. Receives notification of new emergency case invoked by By Stander
   3. Receives phone call from By Stander
   4. Makes phone call to By Stander.
   5. Records Emergency Cases.
   6. Records Ambulance Reservation.
   7. Records irrelevant Emergency case.
   8. Sees list of ambulance, its position and status.
   9. Dispatches ambulance to case scene.
   10. Gets notification when ambulance position and status changed
   11. Record ambulance reservation requested by By Stander
   12. Sees the list of cases, group by kind of case (Emergency, Booking, or Not Relevant)
   13. Sees the list of cases, filtered by status (On Going , Closed)
   14. Searches case by date , caller name or phone number.
   15. Searches patient by medical record number
   16. Searches patient by name and address
   17. Filters search result of no 16 by age or year of birth
   18. Registers new patient.

3. **_Ambulance Nurse_**

   Organization user works as nurse on ambulance crew.

   Requirements

   1. Fill daily checklist at beginning of work shift.
   2. Get notification on new case assigned to the group.
   3. See active case assigned to the group.
   4. Fills pre-hospital care forms:

      - Main Complaint
      - Formerly Disease History
      - Clinical Finding
      - Treatments / actions applied to patient(s)
      - Makes remarks to the case.

4. **_Ambulance Driver_**

   Organization user works as driver on ambulance crew.

   Requirements:

   1. Fill daily checklist at begining of work shift
   2. Get notification on new case assigned to the group
   3. See active case assigned to the group
   4. Sends acknowlegment about the case to call center
   5. Sends ambulance status for each travel phase (Leaving to scene, Arrived at scene, leaving for hospital, arrived at hospital) to call center
   6. Sends other status to call center (Available, Temporary Unavailable, Out of Order)
   7. Can fill ETA for each phase manually
   8. Get geo location of scene when available.
   9. Open Google Map and get route to scene with google map.

5. **_Triage Nurse_**

   Organization user works as nurse at Emergency Room

   Requirements

   1. Get notifcation and information about incoming case
   2. See case details
   3. Print out case details

6. **_Hospital Executives_**

   Organization user works as executives in the hospital that makes schedules, get reports etc.

   Requirements

   1. Sees daily checklist filled by Ambulance Driver, Ambulance Nurse, and Call Center operator
   2. Creates ambulance shift schedule and assigns the group crew
   3. Changes the group crew
   4. Sees active case list and case detail.
   5. See archived cases.
   6. Fiils problem log entry
   7. Sees list of ambulance and it's status.
   8. Fills Power Outtage problem log

7. **_Administrator_**
   Organisation user that able to manage system

   Requirements

   1. Register new user and role
   2. Set up Hospital data entity (name, address, geolocation, etc)
   3. Set up Hospital departements
   4. Set up daily checklist items for user roles
   5. Set up Problem log category
   6. Set up Ambulance data entity.
   7. Set up Pre hospital entity.
      1. Main Complaint
      2. Formerly Disease History
      3. Clinical findings

### Applications

1. Dispatch Management

   1. Type: Web Application Front End
   2. Users: Call Center Operator
   3. Platform: Javascript / Vue

2. Portal

   1. Type: Web Application Front End
   2. Users: Hospital Executives, Administrator
   3. Platform : Javascript / Vue.

3. Single Sign on

   1. Type: Web Application service
   2. Users: all application
   3. Plaform: PHP Laravel

4. ER Room Monitor

   1. Type: Web Application Front End
   2. Users: Hospital Executives, Triage Nurse
   3. Platform: Javascript / Vue

5. Ambulance Driver

   1. Type: Smartphone Application
   2. Users: Ambulance Driver
   3. Platform: Android

6. Pre Hospital Care

   1. Type: Smartphone Application
   2. User: Ambulance Nurse
   3. Platform: Android

7. Emergency Button Controller

   1. Type: Device
   2. User: Bystander
   3. Platform: Arduino

8. Emergency Room Rotary Lamp Controller

   1. Type: Device
   2. Users: Rotary Lamp
   3. Platform: Arduino

9. PABX Controller

   1. Type: Device
   2. Users: Pabx System
   3. Platform: Dotnet Windows Service

10. Vital Sign Controller

    1. Type: Device Controler
    2. User: Vital Sign Device
    3. Platform:

11. GPS Device Controller
    1. Type: Device
    2. User: Gps System
    3. Platform: Sigfox

### Server

1. Restful Api Service

   1. Type: Web Applicaton Back End
   2. Used by: Portal, Dispatch Management, ER Room Monitor, Ambulance Driver, Pre Hospital Care, Web Portal Service, Emergency Button Controller, PABX Controller, GPS device Controller.
   3. Platform: PHP Laravel

2. Web Portal Service

   1. Type: Web Application Back End
   2. Used by: Portal, Dispatch Management, ER Room Monitor, Resftul Api
   3. Platform: PHP Laravel

3. Websocket / Notification Service

   1. Type: Web Application Back End
   2. Used by: Portal, Dispatch Management, ER Room Monitor,
   3. Platform: PHP Ratchet

4. SSO Service

   1. Type: Web Application Back End
   2. Used by: Portal, Dispatch Management, ER Room Monitor, Ambulance Driver, Pre Hospital Care
   3. Platform: PHP Laravel

5. Video Conference Service

   1. Type: Application Back End
   2. Used by: Portal, Dispatch Management, Pre Hospital Care
   3. Platform: Jitsi

6. Database

   1. Type: RDBMS
   2. Used by: Restful Api
   3. Platform: MariaDb

7. Other software
   1. Nginx as webserver, load balancer and reverse proxy
   2. Docker
   3. Git
   4. Composer
   5. Nodejs
